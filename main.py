import os
import json
import requests
from flask import Flask, render_template, request, send_from_directory, jsonify, Response, make_response
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from models import db, Crypto, Exchange
app = Flask(__name__, static_folder="./frontend/build/static", template_folder="./frontend/build")
CORS(app)

'''
crypto = {"Bitcoin":{"name":"Bitcoin","symbol":"BTC","description":"Bitcoin is a cryptocurrency and worldwide payment system. It is the first decentralized digital currency, as the system works without a central bank or single administrator.","exchange_rate_USD":48613,"last24h_market_cap":874954062871,"last24h_volume":53642903329, "icon": "/Bitcoin.png", "exchanges_accepting":["Binance","Coinbase Pro","Gemini"]},\
"Ethereum":{"name":"Ethereum","symbol":"ETH","description":"Ethereum is a decentralized platform for applications. Applications build on it can use smart contracts - computer algorithms which execute themselves when data is supplied to the platform. There is no need for any human operators.","exchange_rate_USD":1826.47,"last24h_market_cap":207367314978,"last24h_volume":22402949580,"icon": "/Ethereum.png","exchanges_accepting":["Binance","Coinbase Pro","Gemini"]},\
"Dogecoin":{"name":"Dogecoin","symbol":"DOGE","description":"Dogecoin is an open source peer-to-peer digital currency, favored by Shiba Inus worldwide.\r\nIntroduced as a \"joke currency\" on 6 December 2013, Dogecoin quickly developed its own online community.","exchange_rate_USD":0.058327,"last24h_market_cap":8492872153,"last24h_volume":1824821984,"icon": "/Dogecoin.png","exchanges_accepting":["Binance"]}}

exchange = {"Coinbase Pro":{"name":"Coinbase Pro","description":"Coinbase Pro is a trading platform for individual traders and crypto enthusiasts. It offers a secure and easy way to buy, sell, and trade digital assets online instantly across various trading pairs.","website_link":"https://pro.coinbase.com/","headquarters":"USA","last24h_volume":3580000000, "icon": "CoinbasePro.jpg", "coins_accepted":["Bitcoin","Ethereum"]},\
"Binance":{"name":"Binance","description":"Binance is a cryptocurrency exchange that provides a platform for trading various cryptocurrencies. As of January 2018, Binance was the largest cryptocurrency exchange in the world in terms of trading volume.","website_link":"https://www.binance.com/en","headquarters":"USA","last24h_volume":25380000000,"icon": "Binance.jpg","coins_accepted":["Bitcoin","Ethereum","Dogecoin"]},\
"Gemini":{"name":"Gemini","description":"Gemini is a regulated cryptocurrency exchange, wallet, and custodian that makes it simple and secure to buy bitcoin, ether, and other cryptocurrencies.","website_link":"https://www.gemini.com/","headquarters":"Malta","last24h_volume":310780000,"icon": "Gemini.jpg","coins_accepted":["Bitcoin","Ethereum"]}}
'''

#app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1234@104.154.151.137:5432/postgres'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
   return render_template("index.html")

@app.route("/api/test")
def test():
      return "TEST123test"

@app.route("/api/cryptocurrencies")
def cryptocurrencies():
    cryptos_dict = {}
    for crypto in db.session.query(Crypto):
        crypto_dict = {}
        crypto_dict['name'] = crypto.name
        crypto_dict['id'] = crypto.id
        crypto_dict['symbol'] = crypto.symbol
        crypto_dict['exchange_rate'] = crypto.exchange_rate
        crypto_dict['market_cap'] = crypto.market_cap
        crypto_dict['volume'] = crypto.volume
        crypto_dict['icon_url'] = crypto.icon_url
        crypto_dict['exchanges_accepting'] = [exchange.name for exchange in Exchange.query.join(Exchange.cryptos).filter(Crypto.id == crypto.id).all()]
        cryptos_dict[crypto.name] = crypto_dict
    return jsonify(cryptos_dict)

@app.route("/api/exchanges")
def exchanges():
    exchanges_dict = {}
    for exchange in db.session.query(Exchange):
        exchange_dict = {}
        exchange_dict['name'] = exchange.name
        exchange_dict['id'] = exchange.id
        exchange_dict['description'] = exchange.description
        exchange_dict['link'] = exchange.link
        exchange_dict['currencies'] = exchange.currencies
        exchange_dict['volume'] = exchange.volume
        exchange_dict['is_active'] = exchange.is_active
        exchange_dict['icon_url'] = exchange.icon_url
        exchange_dict["coins_accepted"] = [crypto.name for crypto in exchange.cryptos]
        exchanges_dict[exchange.name] = exchange_dict
    return jsonify(exchanges_dict)

# external api
@app.route('/cryptos/view') 
def crypto_view(): 
    # fetches all the cryptocurrencies 
    cryptos = Crypto.query.all() 

    response = list()
    for crypto in cryptos: 
        response.append({ 
            "name" : crypto.name,
            "description" : crypto.description, 
            "symbol": crypto.symbol,
            "exchange_rate": crypto.exchange_rate,
            "market_cap": crypto.market_cap,
            "volume": crypto.volume,
            "icon_url": crypto.icon_url,
            "exchanges_accepting": [exchange.name for exchange in Exchange.query.join(Exchange.cryptos).filter(Crypto.id == crypto.id).all()]
        }) 

    return make_response({ 
        'cryptos': response
    }, 200) 

@app.route('/exchanges/view') 
def exchange_view(): 
    # fetches all the exchanges
    exchanges = Exchange.query.all() 

    response = list()
    for exchange in exchanges: 
        response.append({ 
            "name" : exchange.name,
            "description" : exchange.description, 
            "link": exchange.link,
            "currencies": exchange.currencies,
            "is_active": exchange.is_active,
            "volume": exchange.volume,
            "icon_url": exchange.icon_url,
            "coins_accepted": [crypto.name for crypto in exchange.cryptos]
        }) 

    return make_response({ 
        'exchanges': response
    }, 200) 

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, threaded=True, debug=True)
