#!/usr/bin/env python3

# ---------------------------
# projects/cs331e-idb/main.py
# Group 17
# ---------------------------

# -------
# imports
# -------

import os
import sys
import unittest
from models import db, Crypto, Exchange

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    
    def test_crypto_1_insert(self):
        s = Crypto(id='100001', name = 'Cryp')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Crypto).filter_by(id = '100001').one()
        self.assertEqual(str(r.id), '100001')

        db.session.query(Crypto).filter_by(id = '100001').delete()
        db.session.commit()

    def test_crypto_2_exchanges(self):

        r = db.session.query(Crypto).filter_by(id = '3').one()
        all_exchanges = len(Exchange.query.join(Exchange.cryptos).filter(Crypto.id == r.id).all())
        self.assertEqual(all_exchanges, 45)
    
    def test_crypto_3_select_all(self):

        r = db.session.query(Crypto).all()
        self.assertEqual(len(r), 30)
    
    def test_exchange_1_insert(self):
        s = Exchange(id='100001', name = 'Exchange', description = "test exchange")
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Exchange).filter_by(id = '100001').one()
        self.assertEqual(str(r.id), '100001')

        db.session.query(Exchange).filter_by(id = '100001').delete()
        db.session.commit()
    
    def test_exchange_2_all_crypto(self):
        r = db.session.query(Exchange).filter_by(id = '20').one()
        all_crypto = len(r.cryptos)
        print(all_crypto)
        self.assertEqual(all_crypto, 10)
    
    def test_exchange_3_select_all(self):

        r = db.session.query(Exchange).all()
        self.assertEqual(len(r), 50)

    

if __name__ == '__main__':
    unittest.main()
# end of code