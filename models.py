from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1234@104.154.151.137:5432/postgres'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)


link = db.Table('link',
   db.Column('crypto_id', db.Integer, db.ForeignKey('crypto.id')), 
   db.Column('exchange_id', db.Integer, db.ForeignKey('exchange.id'))
   )
  
class Exchange(db.Model):
    __tablename__ = 'exchange'
	
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    description = db.Column(db.String(2000), nullable = True)
    link = db.Column(db.String(250), nullable = True)
    currencies = db.Column(db.Integer,nullable = True)
    volume = db.Column(db.Float, nullable = True)
    is_active = db.Column(db.Boolean, nullable = True)
    icon_url = db.Column(db.String(100), nullable = True)
    

    # This attribute connects an Exchange object, a Crypto object, 
    # and the table link 
    cryptos = db.relationship('Crypto', secondary = 'link', backref='carries')
    
class Crypto(db.Model):
    __tablename__ = 'crypto'
	
    name = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    description = db.Column(db.String(2000), nullable = True)
    symbol = db.Column(db.String(100), nullable = True)
    exchange_rate = db.Column(db.Float, nullable = True)
    market_cap = db.Column(db.Float, nullable = True)
    volume = db.Column(db.Float, nullable = True)
    icon_url = db.Column(db.String(100), nullable = True)

#moved to create_db.py
#db.drop_all()
#db.create_all()
