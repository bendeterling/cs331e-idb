import requests
import json

exchange_response = requests.get('https://api.coinpaprika.com/v1/exchanges')
exchange_json_file = exchange_response.json()
exchange = exchange_json_file[:]
#text = json.dumps(json_file, sort_keys=True, indent=4)

exchange_list = []

exchange_ids = []

id_generator = 1

exchange_id_dict = {}
for i in exchange:
    if i['reported_rank'] and i['reported_rank']<=50:
        exchange_dict = {}
        
        exchange_ids.append(i['id'])
        exchange_id_dict[i['id']] = id_generator
        
        exchange_dict['id'] = id_generator
        id_generator += 1

        exchange_dict['name'] = i['name']
        exchange_dict['description'] = i['description']
        exchange_dict['website'] = i['links']['website'][0]
        exchange_dict['currencies'] = i['currencies']
        exchange_dict['active'] = i['active']
        exchange_dict['volume'] = i['quotes']['USD']['reported_volume_24h']

        exchange_list.append(exchange_dict)

        exchange_dict['logo'] = 'https://static.coinpaprika.com/exchanges/'+i['id']+'/logo.png'
        
response = requests.get('https://api.coinpaprika.com/v1/coins')
json_file = response.json()
crypto = json_file[:30]
#text = json.dumps(json_file, sort_keys=True, indent=4)

crypto_list = []

ids = []

id_generator = 1
for i in crypto:
    crypto_dict = {}
    
    ids.append(i['id'])
    
    crypto_dict['id'] = id_generator
    id_generator += 1

    crypto_dict['name'] = i['name']
    crypto_dict['symbol'] = i['symbol']

    crypto_list.append(crypto_dict)
    

for i, j in enumerate(ids):

    crypto_response = requests.get('https://api.coinpaprika.com/v1/coins/'+j)
    crypto_json_file = crypto_response.json()
    crypto_list[i]['description'] = crypto_json_file['description']

    crypto_market_response = requests.get('https://api.coinpaprika.com/v1/coins/'+j+'/ohlcv/latest')
    crypto_market_json_file = crypto_market_response.json()

    crypto_list[i]['volume'] = crypto_market_json_file[0]['volume']
    crypto_list[i]['market_cap'] = crypto_market_json_file[0]['market_cap']
    
    crypto_list[i]['logo'] = 'https://static.coinpaprika.com/coin/'+j+'/logo.png'

    crypto_exchange_response = requests.get('https://api.coinpaprika.com/v1/coins/'+j+'/markets')
    crypto_exchange_json_file = crypto_exchange_response.json()
    crypto_list[i]['exchanges'] = []
    crypto_list[i]['price'] = crypto_exchange_json_file[0]['quotes']['USD']['price']
    for exchange in crypto_exchange_json_file:
        
        if exchange["exchange_id"] in exchange_ids:
            crypto_list[i]['exchanges'].append(exchange_id_dict[exchange["exchange_id"]])
    


    
