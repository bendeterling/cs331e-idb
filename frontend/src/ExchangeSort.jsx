import React, {useState} from 'react';
import {DropdownButton, Dropdown, ButtonGroup, Button} from "react-bootstrap";

function ExchangeSort({setSortType,setSortOrder,sortExchanges}) {
    const[sortFormatted, setSortFormatted] = useState('Sort by')
    const[sortDirection, setSortDirection] = useState('Order')

    function handleSelectSort(e) {
        setSortType(e)
        switch(e) {
            case 'name':
                setSortFormatted('Name');
                break;
            case 'website_link':
                setSortFormatted('Website Link');
                break;
            case 'description':
                setSortFormatted('Description');
                break;
            case 'is_active':
                setSortFormatted('Trading Status');
                break;
            case 'currencies':
                setSortFormatted('Currencies');
                break;
            case 'volume':
                setSortFormatted('Volume');
                break;
            default:
                setSortFormatted('Sort by')

        }
    }

    function handleSelectDirection(e) {
        setSortOrder(e);
        switch(e){
            case 'ascending':
                setSortDirection('Ascending')
                break;
            case 'descending':
                setSortDirection('Descending')
                break
            default:
                setSortDirection('Order')
        }
    }
    return (
        <div>
            <ButtonGroup style={{paddingLeft:'2%'}}>
                <DropdownButton onSelect={handleSelectSort} title={sortFormatted} as={ButtonGroup}>
                    <Dropdown.Item eventKey="name">Name</Dropdown.Item>
                    <Dropdown.Item eventKey="website_link">Website Link</Dropdown.Item>
                    <Dropdown.Item eventKey="description">Description</Dropdown.Item>
                    <Dropdown.Item eventKey="is_active">Trading Status</Dropdown.Item>
                    <Dropdown.Item eventKey="currencies">Currencies</Dropdown.Item>
                    <Dropdown.Item eventKey="volume">Volume</Dropdown.Item>
                </DropdownButton>
                <DropdownButton onSelect={handleSelectDirection} title={sortDirection} as={ButtonGroup}>
                    <Dropdown.Item eventKey="descending">Descending</Dropdown.Item>
                    <Dropdown.Item eventKey="ascending">Ascending</Dropdown.Item>
                </DropdownButton>
                <Button variant="primary" onClick={()=>sortExchanges()}>Sort!</Button>
            </ButtonGroup>
        </div>
    );
}

export default ExchangeSort;