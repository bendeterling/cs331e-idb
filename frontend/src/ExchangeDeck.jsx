import React, {Component} from 'react';
import ExchangeCard from "./ExchangeCard";
import {CardDeck} from "react-bootstrap";

class ExchangeDeck extends Component {
    render() {
        let cards;
        if(this.props.cards){
            cards = Object.values(this.props.cards).map( card => (
                <ExchangeCard
                    key = {card.name}
                    name = {card.name}
                    description = {card.description}
                    website_link = {card.link}
                    is_active = {card.is_active}
                    volume = {card.volume}
                    coins_accepted = {card.coins_accepted}
                    icon_url = {card.icon_url}
                    currencies = {card.currencies}
                    getDisplayExchange = {this.props.getDisplayExchange}
                />
                )
            )
        }
        return (
            <div>
                <CardDeck style={{paddingLeft:'2%'}}>
                    {cards}
                </CardDeck>
            </div>
        );
    }
}

export default ExchangeDeck;
