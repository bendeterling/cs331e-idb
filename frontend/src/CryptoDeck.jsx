import React, {Component} from 'react';
import CryptoCard from "./CryptoCard";
import {CardDeck} from "react-bootstrap";

class CryptoDeck extends Component {
    render() {
        let cards;
        if(this.props.cards) {
            cards = Object.values(this.props.cards).map(card => (
                    <CryptoCard
                        key={card.name}
                        name={card.name}
                        symbol={card.symbol}
                        exchange_rate={card.exchange_rate}
                        market_cap={card.market_cap}
                        volume={card.volume}
                        exchanges_accepting={card.exchanges_accepting}
                        icon_url={card.icon_url}
                        getDisplayCrypto={this.props.getDisplayCrypto}
                    />
                )
            )
        }
        return (
            <div>
                <CardDeck style={{paddingLeft:'2%'}}>
                    {cards}
                </CardDeck>
            </div>
        );
    }
}

export default CryptoDeck;
