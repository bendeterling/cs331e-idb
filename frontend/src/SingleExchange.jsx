import React from 'react';
import {Button, ListGroup, Image} from 'react-bootstrap';

const SingleExchange = ({setExchangePage, setMainPage, displayExchange,data, getDisplayCrypto}) => {
    let last24h_volume;
    if(data) {
        if (typeof displayExchange === 'string') {
            displayExchange = data[displayExchange]
        }
        console.log(displayExchange.icon_url)
        last24h_volume = displayExchange.volume.toString().split('.')[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+'.'+displayExchange.volume.toString().split('.')[1];
    }
    function displayList() {
        if(data) {
            if (typeof displayExchange === 'string') {
                displayExchange = data[displayExchange]
            }
            return displayExchange.coins_accepted.map(coin => (
                    <ListGroup.Item><Button varient="outline-primary" onClick={() => {
                        setExchangePage('exchanges')
                        setMainPage('cryptocurrencies')
                        getDisplayCrypto(coin)
                    }}>{coin}</Button></ListGroup.Item>
                )
            )
        }
    }
    return (
        <div>
            <div>
                <ListGroup>
                    <ListGroup.Item>Name: {displayExchange.name}  <Image src={displayExchange.icon_url} style={{width: '5rem', height: '5rem'}} rounded /></ListGroup.Item>
                    <ListGroup.Item>Website Link: <a href={displayExchange.website_link}>{displayExchange.website_link}</a></ListGroup.Item>
                    <ListGroup.Item>Description: {displayExchange.description}</ListGroup.Item>
                    <ListGroup.Item>Currently trading: {displayExchange.is_active ? <span style={{color:'green'}}>Yes</span> : <span style={{color:'red'}}>No</span>}</ListGroup.Item>
                    <ListGroup.Item># of currencies accepted: {displayExchange.currencies}</ListGroup.Item>
                    <ListGroup.Item>Volume in last 24h: ${last24h_volume}</ListGroup.Item>
                </ListGroup>
            </div>
            <br/>
            <div style={{paddingLeft:'2%'}}>
                <Button variant='primary' onClick={() => setExchangePage('exchanges')}>See All Exchanges</Button>
            </div>
            <br/>
            <h3 style={{paddingLeft:'2%'}}>Cryptocurrencies that {displayExchange.name} Accepts</h3>
            <div>
                <ListGroup>
                    {displayList()}
                </ListGroup>
            </div>
        </div>


    );
};

export default SingleExchange;
