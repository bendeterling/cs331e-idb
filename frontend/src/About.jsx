import React from 'react';
import './App.css';
import ben from './images/BenDeterling.jpg'
import victor from './images/VictorNguyen.jpg'
import charles from './images/CharlesWood.jpg'
import jeffery from './images/JeffreyYu.jpg'
import xiaoxi from './images/XiaoxiXu.jpg'
import unittests from './images/Unittests.jpg'

const About = () => {

  return (
    <div class = "App-header">
    <link rel="stylesheet" href="/src/App.css" />
    <br/>
    <h2 class = "App" >Track your favorite cryptos with BitTrack!</h2>
    <p>BitTrack is a tool used to find information on the most popular cryptocurrencies and exchanges. BitTrack streamlines the process by using various crypto APIs to provide all the information in one place. Our intended users are both experienced crypto holders to look at stats on coins and exchanges and beginners who just want to learn more. </p>
    <span class = "row justify-content-center">
      <span class = "Card" style={{width: '10rem', margin: '1rem'}}>
        <img src={ben} style={{width: '10rem', height: '10rem'}} alt="BenDeterlingHeadshot"/>
        <h5 class = "card-margin">Ben Deterling</h5>
        <p class = "card-text card-margin">I’m a fourth-year student studying Physics and Plan II as well as the Elements of Computing certificate.<br/><br/><b>Major Responsibilities: </b><br/>React frontend, site navigation and organization, splash page, connection between backend database and frontend, GCP deployment, GCP postgres instance<br/><br/><b>Commits: </b>13<br/><br/><b>Issues: </b>3<br/><br/><b>Unit Tests: </b>0<br/><br/></p>
      </span>
      <span class = "Card" style={{width: '10rem', margin: '1rem'}}>
        <img src={victor} style={{width: '10rem', height: '10rem'}} alt="VictorNguyenHeadshot"/>
        <h5 class = "card-margin">Victor Nguyen</h5>
        <p class = "card-text card-margin">I am a fourth-year student studying Mathematics with a minor in Business, as well as the Elements of Computing certificate.<br/><br/><b>Major Responsibilities: </b><br/>Exchanges page of the Bit Track Website<br/><br/><b>Commits: </b>6<br/><br/><b>Issues: </b>0<br/><br/><b>Unit Tests: </b>0<br/><br/></p>
      </span>
      <span class = "Card" style={{width: '10rem', margin: '1rem'}}>
        <img src={charles} style={{width: '10rem', height: '10rem'}} alt="CharlesWoodHeadshot"/>
        <h5 class = "card-margin">Charles Wood</h5>
        <p class = "card-text card-margin">I’m a third year Civil Engineering major with the Elements of Computing certificate on the side.<br/><br/><b>Major Responsibilities: </b><br/>React frontend, model and instance page linking, sorting, pagination, and searching<br/><br/><b>Commits: </b>24<br/><br/><b>Issues: </b>1<br/><br/><b>Unit Tests: </b>0<br/><br/></p>
      </span>
      <span class = "Card" style={{width: '10rem', margin: '1rem'}}>
        <img src={xiaoxi} style={{width: '10rem', height: '10rem'}} alt="XiaoxiXuHeadshot"/>
        <h5 class = "card-margin">Xiaoxi Xu</h5>
        <p class = "card-text card-margin">I am a fourth year MIS and Philosophy student at UT.<br/><br/><b>Major Responsibilities: </b><br/>Scrape data from the API and build the database<br/><br/><b>Commits: </b>18<br/><br/><b>Issues: </b>36<br/><br/><b>Unit Test: </b>6<br/><br/></p>
      </span>
      <span class = "Card" style={{width: '10rem', margin: '1rem'}}>
        <img src={jeffery} style={{width: '10rem', height: '10rem'}} alt="JeffreyYuHeadshot"/>
        <h5 class = "card-margin">Jeffrey Yu</h5>
        <p class = "card-text card-margin">I'm a junior majoring in MIS with an interest in computer science!<br/><br/><b>Major Responsibilities: </b><br/>About page for the Bit Track website<br/><br/><b>Commits: </b>7<br/><br/><b>Issues: </b>4<br/><br/><b>Unit Tests: </b>0<br/><br/></p>
      </span>
    </span>
    <h3>Stats</h3>
    <ul>
      <li>Total Number of Commits: 62</li>
      <li>Total Number of Issues: 42</li>
      <li>Total Number of Unit Tests: 6</li>
      <li><a href="https://gitlab.com/bendeterling/cs331e-idb/-/issues">GitLab Issue Tracker</a></li>
      <li><a href="https://gitlab.com/bendeterling/cs331e-idb">GitLab Repo</a></li>
      <li><a href="https://gitlab.com/bendeterling/cs331e-idb/-/wikis/Project-1-Technical-Report">GitLab Wiki</a></li>
    </ul>
    <h3>APIs and Data Sources Used</h3>
    <ul>
      <li><a href="https://api.coinpaprika.com/">Coinpaprika API</a></li>
    </ul>
    <h3>Data Scraping</h3>
    <div>
      <p>All data on Bit Track is scraped from <a href="https://api.coinpaprika.com/">Coinpaprika API</a> which is a cryptocurrency market research platform with information of coins and exchanges. The data is obtained by requesting Coinpaprika API and then organized into JSON format to display in frontend.</p> 
    </div>
    <h3>Tools Used</h3>
    <ul>
      <li>React, Flask, Bootstrap, Yarn, Gitlab, Slack</li>
    </ul>
    <h3>Unit Tests</h3>
    <span class = "row justify-content-center">
    <img src={unittests} style={{width: '50rem', height: '10rem'}} alt="Unittests"/>
    {/* <img src={coverage} style={{width: '30rem', height: '30rem'}} alt="Coverage"/> */}
    </span>
    <br/>
    </div>

  );
};

export default About;
