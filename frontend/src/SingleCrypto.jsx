import React from 'react';
// import './App.css';
import {Button, ListGroup, Image} from 'react-bootstrap'


const SingleCrypto = ({setCryptoPage, displayCrypto, setMainPage,getDisplayExchange,data}) => {
    let exchange_rate;
    let last24h_market_cap;
    let last24h_volume;
    if(data) {
        if (typeof displayCrypto === 'string') {
            displayCrypto = data[displayCrypto]
        }
        console.log(displayCrypto.icon_url)
        exchange_rate = displayCrypto.exchange_rate.toString().split('.')[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') +'.'+ displayCrypto.exchange_rate.toString().split('.')[1]
        last24h_market_cap = displayCrypto.market_cap.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        last24h_volume = displayCrypto.volume.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
    function displayList() {
        if(data) {
            if (typeof displayCrypto === 'string') {
                displayCrypto = data[displayCrypto]
            }
            return displayCrypto.exchanges_accepting.map(exchange => (
                    <ListGroup.Item><Button varient="outline-primary" onClick={() => {
                        setCryptoPage('cryptocurrencies')
                        setMainPage('exchanges')
                        getDisplayExchange(exchange)
                    }}>{exchange}</Button></ListGroup.Item>
                )
            )
        }
    }
    return (
    <div>
      <div>
        <ListGroup>
          <ListGroup.Item>Name: {displayCrypto.name} <Image src={displayCrypto.icon_url} style={{width: '5rem', height: '5rem'}} rounded/> </ListGroup.Item>
          <ListGroup.Item>Symbol: {displayCrypto.symbol}</ListGroup.Item>
          <ListGroup.Item>Exchange Rate (to USD): ${exchange_rate}</ListGroup.Item>
          <ListGroup.Item>Market Cap in last 24h: ${last24h_market_cap}</ListGroup.Item>
          <ListGroup.Item>Volume in last 24h: ${last24h_volume}</ListGroup.Item>
        </ListGroup>
      </div>
      <br/>
      <div style={{paddingLeft:'2%'}}>
        <Button variant='primary' onClick={() => setCryptoPage('cryptocurrencies')}>See All Cryptocurrencies</Button>
      </div>
      <br/>
      <h3 style={{paddingLeft:'2%'}}>Exchanges that Accept {displayCrypto.name}</h3>
      <div>
          <ListGroup>
              {displayList()}
          </ListGroup>
      </div>
    </div>
    

  );
};

export default SingleCrypto;
