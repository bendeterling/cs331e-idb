import React from 'react';
import './App.css';
import {Card, Button} from 'react-bootstrap';


const Home = ({setPage}) => {

  return (
    <div>
        <br/>
        <h2 style={{paddingLeft:'2%'}}>Welcome to BitTrack!</h2>
        <span style={{ margin: '3rem',display:'inline-block'}}>
            <Card style={{ width: '18rem'}}>
                <Card.Body>
                <Card.Title>Cryptocurrencies</Card.Title>
                <Card.Text>
                    See information about a wide variety of cryptocurrencies including current price, volume, and listing exchanges.
                </Card.Text>
                <Button variant='outline-primary' onClick={() => setPage('cryptocurrencies')}>Go to Cryptocurrencies</Button>
            </Card.Body>
            </Card>
        </span>
        <span style={{display:'inline-block'}}>
            <Card style={{ width: '18rem'}}>
            <Card.Body>
                <Card.Title>Exchanges</Card.Title>
                <Card.Text>
                    See information about a wide variety of exchanges including current prices, trading volumes, and currencies listed.
                </Card.Text>
                <Button variant='outline-primary' onClick={() => setPage('exchanges')}>Go to Exchanges</Button>
            </Card.Body>
            </Card>
        </span>
    </div>
  );
};

export default Home;
