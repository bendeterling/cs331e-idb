import React from 'react';
import {Pagination} from "react-bootstrap";

function Pages({currentPage,setCurrentPage,totalCards}) {
    let cardsPerPage = 10;
    let items = [];
    for (let num = 1; num <= Math.ceil(totalCards.current/cardsPerPage); num++) {
        items.push(
            <Pagination.Item key={num} active={num === currentPage} onClick={()=>setCurrentPage(num)}>
                {num}
            </Pagination.Item>
        );
    }
    return (
        <div>
            <Pagination style={{paddingLeft:'2%'}}>{items}</Pagination>
        </div>
    );
}

export default Pages;