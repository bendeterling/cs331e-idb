import React, {useState} from 'react';
import {Button, Col, Form} from "react-bootstrap";

function Search({search}) {
    const [searchValue, setSearchValue] = useState('')

    function submit(event) {
        event.preventDefault()
        search(searchValue)
    }

    function reset(event) {
        event.preventDefault()
        setSearchValue('')
        search('')
    }

    return (
        <div>
            <h3 style={{paddingLeft: '2%'}}>Search</h3>
            <Form style={{paddingLeft: '2%'}} >
                <Form.Row className="align-items-center">
                    <Col xs={6}>
                        <Form.Control value={searchValue} onChange={e => setSearchValue(e.target.value)}/>
                        <Form.Text muted>Case sensitive</Form.Text>
                    </Col>
                </Form.Row>
                <Button type="submit" onClick={submit}>Search</Button>{' '}
                <Button type="reset" variant="danger" onClick={reset}>Reset</Button>
            </Form>
        </div>
    );
}

export default Search;