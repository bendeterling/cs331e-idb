import React, {useState} from 'react';
import {DropdownButton, Dropdown, ButtonGroup, Button} from "react-bootstrap";

function CryptoSort({setSortType,setSortOrder,sortCrypto}) {
    const[sortFormatted, setSortFormatted] = useState('Sort by')
    const[sortDirection, setSortDirection] = useState('Order')

    function handleSelectSort(e) {
        setSortType(e)
        switch(e) {
            case 'name':
                setSortFormatted('Name');
                break;
            case 'symbol':
                setSortFormatted('Symbol');
                break;
            case 'exchange_rate':
                setSortFormatted('Exchange Rate');
                break;
            case 'market_cap':
                setSortFormatted('Market Cap');
                break;
            case 'volume':
                setSortFormatted('Volume');
                break;
            default:
                setSortFormatted('Sort by')

        }
    }

    function handleSelectDirection(e) {
        setSortOrder(e);
        switch(e){
            case 'ascending':
                setSortDirection('Ascending')
                break;
            case 'descending':
                setSortDirection('Descending')
                break
            default:
                setSortDirection('Order')
        }
    }

    return (
        <div>
            <ButtonGroup style={{paddingLeft:'2%'}}>
                <DropdownButton onSelect={handleSelectSort} title={sortFormatted} as={ButtonGroup}>
                    <Dropdown.Item eventKey="name">Name</Dropdown.Item>
                    <Dropdown.Item eventKey="symbol">Symbol</Dropdown.Item>
                    <Dropdown.Item eventKey="exchange_rate">Exchange Rate</Dropdown.Item>
                    <Dropdown.Item eventKey="market_cap">Market Cap</Dropdown.Item>
                    <Dropdown.Item eventKey="volume">Volume</Dropdown.Item>
                </DropdownButton>
                <DropdownButton onSelect={handleSelectDirection} title={sortDirection} as={ButtonGroup}>
                    <Dropdown.Item eventKey="descending">Descending</Dropdown.Item>
                    <Dropdown.Item eventKey="ascending">Ascending</Dropdown.Item>
                </DropdownButton>
                <Button variant="primary" onClick={()=>sortCrypto()}>Sort!</Button>
            </ButtonGroup>
        </div>
    );
}

export default CryptoSort;