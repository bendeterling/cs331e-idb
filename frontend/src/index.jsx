import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import Container from './Container';
import * as serviceWorker from './serviceWorker';
import Favicon from 'react-favicon';

ReactDOM.render(
  <React.StrictMode>
    <Favicon url="https://www.favicon.cc/?action=icon&file_id=11386#"></Favicon>
    <Container />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
