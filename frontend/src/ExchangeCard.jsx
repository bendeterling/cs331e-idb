import React, {Component} from 'react';
import {Button, Card, ListGroup} from "react-bootstrap";

class ExchangeCard extends Component {
    render() {
        let last24h_volume = this.props.volume.toString().split('.')[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+'.'+this.props.volume.toString().split('.')[1];
        return (
            <div>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title>{this.props.name}</Card.Title>
                        <Card.Subtitle><a href={this.props.website_link}>{this.props.website_link}</a></Card.Subtitle>
                        <Card.Text>{this.props.description}</Card.Text>
                    </Card.Body>
                    <ListGroup>
                        <ListGroup.Item>Currently trading: {this.props.is_active ? <span style={{color:'green'}}>Yes</span> : <span style={{color:'red'}}>No</span>}</ListGroup.Item>
                        <ListGroup.Item># of currencies accepted: {this.props.currencies}</ListGroup.Item>
                        <ListGroup.Item>Volume in the last 24h: ${last24h_volume}</ListGroup.Item>
                        <ListGroup.Item><Button varient="outline-primary" onClick={() => {
                            this.props.getDisplayExchange(this.props)
                        }}>{this.props.name} Page</Button></ListGroup.Item>
                    </ListGroup>
                </Card>
            </div>
        );
    }
}

export default ExchangeCard;
