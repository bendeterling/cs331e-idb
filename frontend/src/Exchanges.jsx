import React, {useEffect, useRef, useState} from 'react';
import ExchangeDeck from "./ExchangeDeck";
import useAxios from "axios-hooks";
import SingleExchange from './SingleExchange';
import ExchangeSort from "./ExchangeSort";
import {Spinner} from "react-bootstrap";
import Pages from "./Pages";
import Search from "./Search";

function Exchanges({setMainPage,getDisplayExchange,displayExchange,exchangePage,setExchangePage,getDisplayCrypto}) {
    const [{data, loading}]= useAxios("/api/exchanges");
    const [sortType, setSortType] = useState('')
    const [sortOrder, setSortOrder] = useState('')
    const [currentData, setCurrentData] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)
    const [paginatedData, setPaginatedData] = useState(null)
    const [searchedValue, setSearchedValue] = useState('')

    let totalCards = useRef(0);
    let postsPerPage = 10
    if (!loading){
        if (currentData == null){
            setCurrentData(data)
        }
    }

    function sortExchanges(){
        if(sortType==='' || sortOrder==='') return;
        if(currentData){
            let arrayData = Object.values(currentData)
            arrayData.sort(function (a,b) {
                if(a[sortType] < b[sortType]){
                    return -1
                }
                if(a[sortType] > b[sortType]){
                    return 1
                }
                return 0
            })
            if (sortOrder === "descending") arrayData.reverse()
            let sortedData = {}
            arrayData.forEach(e => sortedData[e["name"]] = e)
            setCurrentData(sortedData)
            setCurrentPage(1)
        }
    }

    function search(searchValue) {
        setSearchedValue(searchValue)
    }

    useEffect(() => {
        if(data){
            totalCards.current = Object.keys(data).length
        }
    },[data])

    useEffect(() => {
        const paginate = () => {
            if (currentData) {
                totalCards.current = Object.keys(currentData).length
                let indexOfLastCard = currentPage * postsPerPage
                let indexOfFirstCard = indexOfLastCard - postsPerPage
                let arrayData = Object.values(currentData).slice(indexOfFirstCard, indexOfLastCard)
                let slicedData = {}
                arrayData.forEach(e => slicedData[e["name"]] = e)
                setPaginatedData(slicedData)
            }
        }
        paginate(currentData);
    }, [currentData, postsPerPage, currentPage]);

    useEffect(() => {
        if(data){
            if (searchedValue === ''){
                setCurrentData(data)
                setCurrentPage(1)
            }else{
                let searchedData = {}
                let arrayData = Object.values(data)
                let filteredData = arrayData.filter(e => e['link'].includes(searchedValue)
                    || e['currencies'].toString().includes(searchedValue)
                    || e['name'].includes(searchedValue)
                    || e['description'].includes(searchedValue)
                    || e['volume'].toString().includes(searchedValue)
                    || e['is_active'].toString().includes(searchedValue)
                )
                filteredData.forEach(e => searchedData[e["name"]] = e)
                setCurrentData(searchedData)
                setCurrentPage(1)
            }
        }
    },[searchedValue,data])

    return (
        <div className="exchanges">
            <h2 style={{paddingLeft:'2%'}}>Exchanges</h2>
            {loading  &&  <Spinner animation="border" />}
            {exchangePage === 'exchanges' && <div>
                <ExchangeSort setSortType={setSortType} setSortOrder={setSortOrder} sortExchanges={sortExchanges}/>
                <br/>
                <Search search={search}/>
                <br/>
                <ExchangeDeck cards={paginatedData} getDisplayExchange={getDisplayExchange}/>
                <br/>
                <Pages currentPage={currentPage} setCurrentPage={setCurrentPage} totalCards={totalCards}/>
                {paginatedData != null && Object.keys(paginatedData).length === 0 &&
                <h3 style={{paddingLeft: '2%'}}>Nothing matched your search...</h3>}
            </div>}
            {exchangePage === 'exchange' &&
            <SingleExchange setExchangePage={setExchangePage} setMainPage={setMainPage}
                            displayExchange={displayExchange} data={data} getDisplayCrypto={getDisplayCrypto}/>}
        </div>
    );
}

export default Exchanges;
