import React, {Component} from 'react';
import {Button, Card, ListGroup} from "react-bootstrap";

class CryptoCard extends Component {
    render() {
        let exchange_rate = this.props.exchange_rate.toString().split('.')[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        if (this.props.exchange_rate.toString().includes('.')){
            exchange_rate = exchange_rate  +'.'+ this.props.exchange_rate.toString().split('.')[1]
        }
        let last24h_market_cap = this.props.market_cap.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        let last24h_volume = this.props.volume.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        return (
            <div>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title>{this.props.name}</Card.Title>
                        <Card.Subtitle>{this.props.symbol}</Card.Subtitle>
                    </Card.Body>
                    <ListGroup>
                        <ListGroup.Item>Current Exchange Rate: ${exchange_rate}</ListGroup.Item>
                        <ListGroup.Item>Market Cap in the last 24h: ${last24h_market_cap}</ListGroup.Item>
                        <ListGroup.Item>Volume in the last 24h: ${last24h_volume}</ListGroup.Item>
                        <ListGroup.Item><Button varient="outline-primary" onClick={() => {
                            this.props.getDisplayCrypto(this.props)
                        }}>{this.props.name} Page</Button></ListGroup.Item>
                    </ListGroup>
                </Card>
            </div>
        );
    }
}

export default CryptoCard;
