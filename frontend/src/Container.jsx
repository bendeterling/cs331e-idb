import React,{useState} from 'react';
import './App.css';
import Home from './Home.jsx';
import About from './About.jsx';
import Exchanges from './Exchanges.jsx';
import Cryptocurrencies from "./Cryptocurrencies";
import {Nav, Navbar} from 'react-bootstrap';

function Container() {
  const[page, setPage] = useState('home');
  const[exchangePage, setExchangePage] = useState('exchanges');
  const[displayExchange, setDisplayExchange] = useState(null)
  const getDisplayExchange = (index) =>{
    setExchangePage('exchange')
    setDisplayExchange(index)
  }
  const[cryptoPage, setCryptoPage] = useState('cryptocurrencies');
  const[displayCrypto, setDisplayCrypto] = useState(null)
  const getDisplayCrypto = (index) =>{
    setCryptoPage('cryptocurrency')
    setDisplayCrypto(index)
  }
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand onClick={() => setPage('home')}>Bit Track</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link onClick={() => {
              setCryptoPage('cryptocurrencies')
              setExchangePage('exchanges')
              setPage('home')
            }}>Home</Nav.Link>
            <Nav.Link onClick={() => {
              setCryptoPage('cryptocurrencies')
              setExchangePage('exchanges')
              setPage('about')
            }}>About</Nav.Link>
            <Nav.Link onClick={() => {
              setCryptoPage('cryptocurrencies')
              setExchangePage('exchanges')
              setPage('cryptocurrencies')
            }}>Cryptocurrencies</Nav.Link>
            <Nav.Link onClick={() => {
              setCryptoPage('cryptocurrencies')
              setExchangePage('exchanges')
              setPage('exchanges')
            }}>Exchanges</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
        <div>
            {page === 'home' && <Home setPage={setPage}/>}
            {page === 'about' && <About />}
            {page === 'cryptocurrencies' && <Cryptocurrencies setMainPage={setPage} getDisplayExchange={getDisplayExchange} getDisplayCrypto={getDisplayCrypto} displayCrypto={displayCrypto} cryptoPage={cryptoPage} setCryptoPage={setCryptoPage}/>}
            {page === 'exchanges' && <Exchanges setMainPage={setPage} getDisplayExchange={getDisplayExchange} displayExchange={displayExchange} exchangePage={exchangePage} setExchangePage={setExchangePage} getDisplayCrypto={getDisplayCrypto}/>}
        </div>
    </div>
  )
}

export default Container;
