from models import app, db, Exchange, Crypto
from data import crypto_list, exchange_list

def create_cryptos():
    #drop and create tables on load
    db.drop_all()
    db.create_all()

    # create exchange objects

    for i in exchange_list:
        exchange = Exchange(id = i['id'], name = i['name'], description = i['description'], \
                            link = i['website'], currencies = i['currencies'], is_active = i['active'], \
                            volume = i['volume'], icon_url = i['logo'])
        db.session.add(exchange)

    db.session.commit()

    # create crypto objects
    for i in crypto_list:
        crypto = Crypto(id = i['id'], name = i['name'], description = i['description'], \
                            symbol = i['symbol'], exchange_rate = i['price'], market_cap = i['market_cap'], \
                            volume = i['volume'], icon_url = i['logo'])
        
        db.session.add(crypto)
        db.session.commit() 

        for j in i['exchanges']:
            someExchange = Exchange.query.filter_by(id=j).first()
            crypto.carries.append(someExchange)

        db.session.commit()
    
    
    # Querying
    #---------
    '''
    someAuthor = Author.query.filter_by(name='author1').first()
    print("Author data:")
    print(someAuthor)
    # someAuthor is an object. We can access its id, name and books attributes using 
    # the dot operator.
    print(someAuthor.id)
    print(someAuthor.name)
    '''
    '''
    print(someAuthor.books)
    print(someAuthor.books[0])
    print(someAuthor.books[0].id)
    '''
    '''
    someBook = Book.query.filter_by(title='book1').first()
    print("Book data:")
    print(someBook)
    # someBook is an object. We can obtain its id and title using the dot operator.
    print(someBook.id)
    print(someBook.title)
    print(someBook.wrote)
    print(someBook.wrote[0])
    print(someBook.wrote[0].id)
    
    for author in someBook.wrote:
       print(author.name)
    '''
'''   
    # add an Author object to the author attribute of Book object.
    book1.wrote.append(author2)
    book2.wrote.append(author3)
    db.session.add(book1)
    db.session.add(book2)
    db.session.commit()
    
    author4 = Author(name = 'author4', id = 4)
    author4.books.append(book1)
    db.session.add(author4)
    db.session.commit()
'''    
    
    
create_cryptos()
